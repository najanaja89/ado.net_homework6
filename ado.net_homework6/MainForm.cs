﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ado.net_homework6;
using ado.net_homework6.DataAccessLevel;

namespace ado.net_homework6
{
    public partial class MainForm : Form
    {
        DataAccess dataAccess = new DataAccess();

        public MainForm()
        {
            InitializeComponent();
            filterUsers(false);

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Add User
            AddUserForm form = new AddUserForm();
            form.Show();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                filterUsers(true);
            else
                filterUsers(false);
        }

        private void filterUsers(bool isAdmin)
        {
            var listUsers = dataAccess.UsersTableGetListUsers();
            listBox1.Items.Clear();
            if (isAdmin)
            {
                foreach (var item in listUsers)
                {
                    if (item.IsAdmin == "True")
                    {
                        listBox1.Items.Add(item.Login);
                    }
                }
            }
            else
            {
                foreach (var item in listUsers)
                {
                    listBox1.Items.Add(item.Login);
                }
            }
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            EditForm form = new EditForm(listBox1.Text);
            form.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DeleteForm form = new DeleteForm();
            form.Show();
        }
    }
}
