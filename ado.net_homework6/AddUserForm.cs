﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ado.net_homework6.DataAccessLevel;

namespace ado.net_homework6
{
    public partial class AddUserForm : Form
    {
        public AddUserForm()
        {
            InitializeComponent();
            textBox2.Text = "";
            // The password character is an asterisk.  
            textBox2.PasswordChar = '*';
            // The control will allow no more than 14 characters.  
            textBox2.MaxLength = 14;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //Login
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //password
        }

        private void button1_Click(object sender, EventArgs e)
        {

            DataAccess dataAccess = new DataAccess();
            if (dataAccess.IsUserExist(textBox1.Text))
            {
                MessageBox.Show("User is exists");
            }
            else
            {
                bool isAdmin;
                if (checkBox1.Checked)
                {
                    isAdmin = true;
                }
                else
                {
                    isAdmin = false;
                }

                dataAccess.AddUser(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, isAdmin);
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            //address
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            //phone
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void AddUserForm_Load(object sender, EventArgs e)
        {

        }
    }
}
