﻿using ado.net_homework6.DataAccessLevel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ado.net_homework6
{
    public partial class DeleteForm : Form
    {
        public DeleteForm()
        {
            InitializeComponent();
        }

        private void DeleteForm_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataAccess dataAccess = new DataAccess();
            if (dataAccess.IsUserExist(textBox1.Text))
            {
                dataAccess.DeleteUser(textBox1.Text);
            }
            else MessageBox.Show("User not exist");
        }
    }
}
