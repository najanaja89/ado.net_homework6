﻿using ado.net_homework6.DataAccessLevel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ado.net_homework6
{
    public partial class EditForm : Form
    {
        public EditForm(string outLogin)
        {
            InitializeComponent();
            DataAccess dataAccess = new DataAccess();
            textBox1.Text = outLogin;
            textBox2.Text = "";
            // The password character is an asterisk.  
            textBox2.PasswordChar = '*';
            // The control will allow no more than 14 characters.  
            textBox2.MaxLength = 14;

            textBox3.Text = dataAccess.FindUser(outLogin).Address;
            textBox4.Text = dataAccess.FindUser(outLogin).PhoneNumber;
            if (dataAccess.FindUser(outLogin).IsAdmin.ToLower()=="true") checkBox1.Checked = true;
            else checkBox1.Checked = false;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            //Address
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            //PhoneNumber
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //Isadmin
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //Login
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //Password
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataAccess dataAccess = new DataAccess();
            if (dataAccess.IsUserExist(textBox1.Text))
            {
                bool isAdmin;
                if (checkBox1.Checked)
                {
                    isAdmin = true;
                }
                else
                {
                    isAdmin = false;
                }
                dataAccess.EditUser(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, isAdmin);
                MessageBox.Show("User Edited");

            }
            else
            {
                MessageBox.Show("User not exists");
            }
        }

        private void EditForm_Load(object sender, EventArgs e)
        {

        }
    }
}
