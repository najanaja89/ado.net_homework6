﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Configuration;
using ado.net_homework6.Models;

namespace ado.net_homework6.DataAccessLevel
{
    public class DataAccess
    {
        public List<User> UsersTableGetListUsers()
        {
            //User user = new User();
            string login = "";
            string password = "";
            string address = "";
            string phoneNumber = "";
            string isAdmin = "";
            List<User> users = new List<User>();
            var configuration = ConfigurationManager.ConnectionStrings["appConnection"];
            var providerName = configuration.ProviderName;
            var connectionString = configuration.ConnectionString;

            var providerFactory = DbProviderFactories.GetFactory(providerName);

            using (var connection = providerFactory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                var dataSet = new DataSet("users");
                var dataAdapter = providerFactory.CreateDataAdapter();

                var selectUsersCommand = connection.CreateCommand();
                selectUsersCommand.CommandText = "select * from Users";
                dataAdapter.SelectCommand = selectUsersCommand;

                dataAdapter.Fill(dataSet, "Users");

                var UserTablesData = dataSet.Tables["Users"];
                foreach (DataRow row in UserTablesData.Rows)
                {
                    foreach (DataColumn column in UserTablesData.Columns)
                    {
                        if (column.ColumnName.ToString() == "Login") login = row[column].ToString();
                        if (column.ColumnName.ToString() == "Password") password = row[column].ToString();
                        if (column.ColumnName.ToString() == "Address") address = row[column].ToString();
                        if (column.ColumnName.ToString() == "PhoneNumber") phoneNumber = row[column].ToString();
                        if (column.ColumnName.ToString() == "IsAdmin") isAdmin = row[column].ToString();
                    }
                    users.Add(new User
                    {
                        Login = login,
                        Password = password,
                        Address = address,
                        PhoneNumber = phoneNumber,
                        IsAdmin = isAdmin
                    });
                }
                return users;
            }
        }

        public bool IsUserExist(string newLogin)
        {
            var configuration = ConfigurationManager.ConnectionStrings["appConnection"];
            var providerName = configuration.ProviderName;
            var connectionString = configuration.ConnectionString;

            var providerFactory = DbProviderFactories.GetFactory(providerName);

            using (var connection = providerFactory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                var dataSet = new DataSet("users");
                var dataAdapter = providerFactory.CreateDataAdapter();

                var selectUsersCommand = connection.CreateCommand();
                selectUsersCommand.CommandText = "select Login from Users";
                dataAdapter.SelectCommand = selectUsersCommand;

                dataAdapter.Fill(dataSet, "Users");

                var logins = dataSet.Tables["Users"];

                foreach (DataRow dataRow in logins.Rows)
                {
                    for (int i = 0; i < logins.Columns.Count; i++)
                    {
                        if (newLogin == dataRow[logins.Columns[i].ColumnName].ToString())
                        {
                            return true;
                        }

                    }
                }
            }
            return false;
        }

        public void AddUser(string newLogin, string newPassword, string newAddress, string newPhone, bool isAdmin)
        {
            var configuration = ConfigurationManager.ConnectionStrings["appConnection"];
            var providerName = configuration.ProviderName;
            var connectionString = configuration.ConnectionString;

            var providerFactory = DbProviderFactories.GetFactory(providerName);

            using (var connection = providerFactory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                var dataSet = new DataSet("users");
                var dataAdapter = providerFactory.CreateDataAdapter();

                var selectUsersCommand = connection.CreateCommand();
                selectUsersCommand.CommandText = "select * from Users";
                dataAdapter.SelectCommand = selectUsersCommand;

                dataAdapter.Fill(dataSet, "Users");

                var commandBuilder = providerFactory.CreateCommandBuilder();
                commandBuilder.DataAdapter = dataAdapter;

                var usersTable = dataSet.Tables["Users"];
                var row = usersTable.NewRow();
                row["Login"] = newLogin.ToLower();
                row["Password"] = newPassword.ToLower().GetHashCode();
                row["Address"] = newAddress.ToLower();
                row["PhoneNumber"] = newPhone.ToLower();
                row["IsAdmin"] = isAdmin;
                usersTable.Rows.Add(row);

                dataAdapter.Update(dataSet, "Users");

            }
        }

        public User FindUser(string findLogin)
        {
            User user = new User();
            var configuration = ConfigurationManager.ConnectionStrings["appConnection"];
            var providerName = configuration.ProviderName;
            var connectionString = configuration.ConnectionString;

            var providerFactory = DbProviderFactories.GetFactory(providerName);

            using (var connection = providerFactory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                var dataSet = new DataSet("users");
                var dataAdapter = providerFactory.CreateDataAdapter();

                var selectUsersCommand = connection.CreateCommand();
                selectUsersCommand.CommandText = $"select * from users";
                dataAdapter.SelectCommand = selectUsersCommand;

                dataAdapter.Fill(dataSet, "Users");

                var commandBuilder = providerFactory.CreateCommandBuilder();
                commandBuilder.DataAdapter = dataAdapter;

                var usersTable = dataSet.Tables["Users"];
                var rows = usersTable.Rows;
                foreach (DataRow dRow in rows)
                {
                    if (dRow["Login"].ToString() == findLogin)
                    {

                        user.Login = dRow["Login"].ToString();
                        user.Password = dRow["Password"].ToString();
                        user.Address = dRow["Address"].ToString();
                        user.PhoneNumber = dRow["PhoneNumber"].ToString();
                        user.IsAdmin = dRow["IsAdmin"].ToString();
                    }
                }

                return user;
            }
        }

        public void DeleteUser(string deleteLogin)
        {
            var configuration = ConfigurationManager.ConnectionStrings["appConnection"];
            var providerName = configuration.ProviderName;
            var connectionString = configuration.ConnectionString;

            var providerFactory = DbProviderFactories.GetFactory(providerName);

            using (var connection = providerFactory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                var dataSet = new DataSet("users");
                var dataAdapter = providerFactory.CreateDataAdapter();

                var selectUsersCommand = connection.CreateCommand();
                selectUsersCommand.CommandText = $"select * from users";
                dataAdapter.SelectCommand = selectUsersCommand;

                dataAdapter.Fill(dataSet, "Users");

                var commandBuilder = providerFactory.CreateCommandBuilder();
                commandBuilder.DataAdapter = dataAdapter;

                var usersTable = dataSet.Tables["Users"];
                var rows = usersTable.Rows;
                foreach (DataRow dRow in rows)
                {
                    if (dRow["Login"].ToString() == deleteLogin)
                    {
                        dRow.Delete();
                    }
                }

                dataAdapter.Update(dataSet, "Users");
            }
        }

        public void EditUser(string newLogin, string newPassword, string newAddress, string newPhone, bool isAdmin)
        {
            var configuration = ConfigurationManager.ConnectionStrings["appConnection"];
            var providerName = configuration.ProviderName;
            var connectionString = configuration.ConnectionString;

            var providerFactory = DbProviderFactories.GetFactory(providerName);

            using (var connection = providerFactory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                var dataSet = new DataSet("users");
                var dataAdapter = providerFactory.CreateDataAdapter();

                var selectUsersCommand = connection.CreateCommand();
                selectUsersCommand.CommandText = $"select * from users";
                dataAdapter.SelectCommand = selectUsersCommand;

                dataAdapter.Fill(dataSet, "Users");

                var commandBuilder = providerFactory.CreateCommandBuilder();
                commandBuilder.DataAdapter = dataAdapter;

                var usersTable = dataSet.Tables["Users"];
                var rows = usersTable.Rows;
                foreach (DataRow dRow in rows)
                {
                    if (dRow["Login"].ToString() == newLogin)
                    {
                        dRow.BeginEdit();
                        dRow["Login"] = newLogin;
                        dRow["Password"] = newPassword;
                        dRow["Address"] = newAddress;
                        dRow["PhoneNumber"] = newPhone;
                        dRow["IsAdmin"] = isAdmin;
                        dRow.EndEdit();
                    }
                }

                dataAdapter.Update(dataSet, "Users");
            }
        }
    }
}