﻿using ado.net_homework6.DataAccessLevel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework6.DebugConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            DataAccess dataAccess = new DataAccess();
            var UserTablesData = dataAccess.UsersTableGetLogins().Tables["Users"];

            foreach (DataRow row in UserTablesData.Rows)
            {
                foreach (DataColumn column in UserTablesData.Columns)
                {
                    //Console.Write("Item: ");
                    Console.WriteLine(column.ColumnName);
                    //Console.Write(" ");
                    //Console.WriteLine(row[column]);

                } 
            }

            Console.ReadLine();
        }
    }
}
